from django.contrib import admin

from movie.models import Currency, RecentMovie

class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('code', 'value', 'created_at')

class RecentMovieAdmin(admin.ModelAdmin):
    list_display = ('title', 'img')

admin.site.register(Currency, CurrencyAdmin)
admin.site.register(RecentMovie, RecentMovieAdmin)
