from django.apps import AppConfig


class InfomoviesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'infomovies'
