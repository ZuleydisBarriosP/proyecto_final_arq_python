from django.urls import path
from infomovies.views import ping, MoviesList, MoviesDetails


urlpatterns = [
    #path('', HomeViewMovie.as_view(), name='home'),
    path("status/", ping, name="ping"),
    path("api/movies/", MoviesList.as_view(), name= "api_movies"),
    path("api/movies/<int:pk>/", MoviesDetails.as_view()),
]
