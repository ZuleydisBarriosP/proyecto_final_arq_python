from infomovies.serializers import MovieSerializer

def test_valid_movie_serializer():
    valid_serializer_data = {
        "title": "The Hunger Games",
        "year": 2012,
        "genre": "Sci-Fi",
        "classification": "PG-13",
    }
    serializer = MovieSerializer(data=valid_serializer_data)
    assert serializer.is_valid()
    assert serializer.validated_data == valid_serializer_data
    assert serializer.data == valid_serializer_data
    assert serializer.errors == {}

def test_invalid_movie_serializer():
    invalid_serializer_data = {
        "title": "I am legend",
        "year": 2007,
    }
    serializer = MovieSerializer(data=invalid_serializer_data)
    assert not serializer.is_valid()
    assert serializer.validated_data == {}
    assert serializer.data == invalid_serializer_data
    assert serializer.errors == {
        "genre": ["This field is required."],
        "classification": ["This field is required."],
    }