import pytest
from infomovies.models import Movies

@pytest.mark.django_db
def test_movies_model():
    movie = Movies(
        title="The fast and the furious",
        genre="Action",
        year=2008,
        classification="PG-13"
    )
    movie.save()

    assert movie.title == "The fast and the furious"
    assert movie.genre == "Action"
    assert movie.year == 2008
    assert movie.classification == "PG-13"
    assert movie.created_at
    assert movie.updated_at
    assert str(movie) == movie.title