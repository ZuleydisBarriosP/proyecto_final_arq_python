# Arq Python
This project was created as final work of python architecture.
This project makes scraper to the page of cinecolombia executing some tasks in celery, it saves this information in the database and it is visualized in the web page.

# members
Jorge Zetien

Zuleydis Barrios

# Instructions for running the project
Compiling from source requires Python 3 installed, being tested only under Python 3.9. It is recommended to run the following steps in a virtual environment:

# Clone Project
- git clone https://gitlab.com/ZuleydisBarriosP/proyecto_final_arq_python.git

![img](https://gitlab.com/ZuleydisBarriosP/proyecto_final_arq_python/-/raw/devel/capturasimg/clonarelproyecto.png)

# Install dependencies
- `pip install -r requirements.txt`

# Compile
- `python manage.py migrate`
- `python manage.py runserver`


# Coverage
To execute it, the following command must be used, it must be with pytest and not with test

- `coverage run -m pytest`
- `coverage report`
- `coverage html`

### Resultado Ejecucion coverage report 
![img](https://gitlab.com/ZuleydisBarriosP/proyecto_final_arq_python/-/raw/devel/capturasimg/Ejecucion_coverage.png)


# Celery
- Have active rabbitmq
![img](https://gitlab.com/ZuleydisBarriosP/proyecto_final_arq_python/-/raw/devel/capturasimg/rabbitactivo.png)

- `celery -A arq_python2 worker -l info`
- `celery -A arq_python2 beat -l info`
### Celery en ejecucion
![img](https://gitlab.com/ZuleydisBarriosP/proyecto_final_arq_python/-/raw/devel/capturasimg/celery.png)


### Django RestFramework
![img](https://gitlab.com/ZuleydisBarriosP/proyecto_final_arq_python/-/raw/devel/capturasimg/microservicios.png)


### *Specs*

| **key**                      | **value**              | 
|------------------------------|------------------------|
| Requirements path            | requirements.txt       |
| Settings path                | arq_python/settings.py |
| Python version               | 3.9.7                  |
| Django version               | 4.0.3                  |



### *Bookstores*
| **key**                      | **value**              | 
|------------------------------|------------------------|
| beautifulsoup4               | 4.10.0                 |
| celery                       | 5.2.6                  |
| coverage                     | 6.3.2                  |
| flower                       | 1.0.0                  |
| pytest                       | 7.1.1                  |
| requests                     | 2.27.1                 |
| SQLAlchemy                   | 1.4.36                 |

